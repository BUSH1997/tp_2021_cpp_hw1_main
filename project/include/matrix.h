#ifndef PROJECT_INCLUDE_MATRIX_H_
#define PROJECT_INCLUDE_MATRIX_H_

#include <stdio.h>

#define BUFFER_COEFFICIENT 2

typedef struct Matrix {
    int** data;
    size_t rows;
    size_t* cols;
} Matrix;

void free_matrix(Matrix** matrix);

Matrix* cut_odd_elements(Matrix* matrix);

Matrix* create_matrix_from_input(FILE* file);

#endif  // PROJECT_INCLUDE_MATRIX_H_
