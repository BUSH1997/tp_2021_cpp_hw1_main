#include "matrix.h"

#include <stdlib.h>

int main() {
    Matrix* matrix = create_matrix_from_input(stdin);

    if (!matrix) {
        return -1;
    }

    Matrix* new_matrix = cut_odd_elements(matrix);

    if (!new_matrix) {
        free_matrix(&matrix);
        return -1;
    }

    for (size_t i = 0; i < new_matrix->rows; ++i) {
        for (size_t j = 0; j < new_matrix->cols[i]; ++j) {
            printf("%d ", new_matrix->data[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for (size_t i = 0; i < new_matrix->rows; ++i) {
        printf("%zu ", new_matrix->cols[i]);
    }

    free_matrix(&new_matrix);
    free_matrix(&matrix);

    return 0;
}
