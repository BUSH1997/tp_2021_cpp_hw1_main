#include "matrix.h"

#include <stdlib.h>
#include <string.h>

void free_matrix(Matrix** matrix) {
    if (!(*matrix)) {
        return;
    }

    for (size_t i = 0; i < (*matrix)->rows; ++i) {
        free((*matrix)->data[i]);
    }

    free((*matrix)->data);
    free((*matrix)->cols);
    free((*matrix));

    *matrix = NULL;
}

static Matrix* create_matrix(size_t rows, size_t cols) {
    Matrix* matrix = (Matrix*)malloc (sizeof (Matrix));

    if (!matrix) {
        return NULL;
    }

    matrix->rows = rows;

    matrix->cols = (size_t*)calloc(sizeof(size_t), rows);

    if (!matrix->cols) {
        free(matrix);
        return NULL;
    }

    for (size_t i = 0; i < rows; ++i) {
        matrix->cols[i] = cols;
    }

    matrix->data = (int**) malloc (sizeof (int*) * rows);
    if (!matrix->data) {
        free(matrix->cols);
        free(matrix);
        return NULL;
    }
    return matrix;
}

static Matrix* allocate_rows_memory(Matrix* matrix) {
    for (size_t i = 0; i < matrix->rows; ++i) {
        matrix->data[i] = (int*) calloc(sizeof(int), matrix->cols[i]);
        if (!matrix->data[i]) {
            return NULL;
        }
    }
    return matrix;
}

Matrix* create_matrix_from_input(FILE* file) {
    if (!file) {
        perror("Ошибка создания матрицы");
        return NULL;
    }

    size_t rows = 0;
    size_t cols = 0;

    if (fscanf(file, "%zu%zu", &rows, &cols) != 2) {
        printf("Ошибка создания матрицы: невалидные входные данные\n");
        return NULL;
    }

    if (!rows || !cols) {
        printf("Ошибка создания матрицы: невалидные входные данные\n");
        return NULL;
    }

    Matrix* matrix = create_matrix(rows, cols);

    if (!matrix) {
        perror("Ошибка создания матрицы");
        return NULL;
    }

    if (!allocate_rows_memory(matrix)) {
        perror("Ошибка создания матрицы");
        free_matrix(&matrix);
        return NULL;
    }

    for (size_t i = 0; i < matrix->rows; ++i) {
        for (size_t j = 0; j < matrix->cols[i]; ++j) {
            int scan_result = fscanf(file, "%d", &matrix->data[i][j]);

            if (scan_result != 1 || scan_result == EOF) {
                printf("Ошибка создания матрицы: невалидные входные данные\n");
                free_matrix(&matrix);
                return NULL;
            }
        }
    }

    return matrix;
}

static int safe_realloc(Matrix* matrix, size_t i, size_t size) {
    int *matrix_p = (int*)realloc(matrix->data[i], sizeof(int) * size);

    if (!matrix_p) {
        return -1;
    }

    matrix->data[i] = matrix_p;

    matrix_p = NULL;

    return 0;
}

Matrix* cut_odd_elements(Matrix* matrix) {
    if (!matrix->rows) {
        printf("Ошибка удаления нечетных элементов: невалидные входные данные\n");
        return NULL;
    }

    Matrix* output_matrix = create_matrix(matrix->rows, matrix->cols[0]);

    if (!output_matrix) {
        perror("Ошибка удаления нечетных элементов");
        return NULL;
    }

    for (size_t i = 0; i < matrix->rows; ++i) {
        size_t buffer_length = 0;

        size_t buffer_capacity = 1;

        output_matrix->data[i] = NULL;

        for (size_t j = 0; j < matrix->cols[i]; ++j) {
            if (matrix->data[i][j] % 2 != 1) {
                if (buffer_length == buffer_capacity - 1) {
                    buffer_capacity *= BUFFER_COEFFICIENT;

                    if (safe_realloc(output_matrix, i, buffer_capacity)) {
                        perror("Ошибка удаления нечетных элементов");
                        free_matrix(&output_matrix);
                        return NULL;
                    }
                }

                output_matrix->data[i][buffer_length] = matrix->data[i][j];

                ++buffer_length;
            }
        }

        output_matrix->cols[i] = buffer_length;
    }

    for (size_t i = 0; i < output_matrix->rows; ++i) {
        if (safe_realloc(output_matrix, i, output_matrix->cols[i])) {
            perror("Ошибка удаления нечетных элементов");
            free_matrix(&output_matrix);
            return NULL;
        }
    }

    return output_matrix;
}
