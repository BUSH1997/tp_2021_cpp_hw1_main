#include "gtest/gtest.h"

extern "C" {
    #include "matrix.h"
}

Matrix* cut_matrix_creation(FILE* f) {
    Matrix* cut_matrix = (Matrix*)malloc(sizeof(Matrix));

    fscanf(f, "%zu", &cut_matrix->rows);

    cut_matrix->cols = (size_t*)calloc(cut_matrix->rows, sizeof(size_t));

    for (size_t i = 0; i < cut_matrix->rows; ++i) {
        fscanf(f, "%zu", &cut_matrix->cols[i]);
    }

    cut_matrix->data = (int**)malloc(sizeof(int*) * cut_matrix->rows);

    for (size_t i = 0; i < cut_matrix->rows; ++i) {
        cut_matrix->data[i] = (int*)calloc(sizeof(int), cut_matrix->cols[i]);
    }

    for (size_t i = 0; i < cut_matrix->rows; ++i) {
        for (size_t j = 0; j < cut_matrix->cols[i]; ++j) {
             fscanf(f, "%d", &cut_matrix->data[i][j]);
        }
    }

    return cut_matrix;
}

bool cut_matrix_assertion(Matrix* cut_matrix_1, Matrix* cut_matrix_2) {
    if (cut_matrix_1->rows != cut_matrix_2->rows) {
        return false;
    }

    for (size_t i = 0; i < cut_matrix_2->rows; ++i) {
        if (cut_matrix_1->cols[i] != cut_matrix_2->cols[i]) {
            return false;
        }
    }

    for (size_t i = 0; i < cut_matrix_2->rows; ++i) {
        for (size_t j = 0; j < cut_matrix_2->cols[i]; ++j) {
             if (cut_matrix_1->data[i][j] != cut_matrix_2->data[i][j]) {
                return false;
             }
        }
    }

    return true;
}

TEST(matrix_creation, no_file) {
    FILE* f = fopen("../tests/test_data/no_file.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_creation, good_creation) {
    FILE* f = fopen("../tests/test_data/matrix_creation/testdata_creation.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_NE(matrix, nullptr);

    free_matrix(&matrix);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_creation, not_enoght_data) {
    FILE* f = fopen("../tests/test_data/matrix_creation/testdata_creation_corrupt1.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_creation, symbol_1) {
    FILE* f = fopen("../tests/test_data/matrix_creation/testdata_creation_corrupt2.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_creation, symbol_2) {
    FILE* f = fopen("../tests/test_data/matrix_creation/testdata_creation_corrupt3.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_creation, empty_file) {
    FILE* f = fopen("../tests/test_data/matrix_creation/testdata_creation_corrupt4.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_destruction, free) {
    FILE* f = fopen("../tests/test_data/free_matrix/testdata_matrix_destruction.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    free_matrix(&matrix);

    EXPECT_EQ(matrix, nullptr);

    if (f) {
        fclose(f);
    }
}

TEST(matrix_destruction, no_free) {
    FILE* f = fopen("../tests/test_data/free_matrix/testdata_matrix_destruction.dat", "r");

    Matrix* matrix = create_matrix_from_input(f);

    EXPECT_NE(matrix, nullptr);

    free_matrix(&matrix);

    if (f) {
        fclose(f);
    }
}


TEST(cut_matrix_creation, success) {
    FILE* f_in = fopen("../tests/test_data/cut_odd_elements/testdata_cut_in1.dat", "r");

    Matrix* matrix = create_matrix_from_input(f_in);

    Matrix* cut_matrix_1 = cut_odd_elements(matrix);

    FILE* f_out = fopen("../tests/test_data/cut_odd_elements/testdata_cut_out1.dat", "r");

    Matrix* cut_matrix_2 = cut_matrix_creation(f_out);

    EXPECT_TRUE(cut_matrix_assertion(cut_matrix_1, cut_matrix_2));

    free_matrix(&cut_matrix_1);
    free_matrix(&cut_matrix_2);
    free_matrix(&matrix);

    if (f_in) {
        fclose(f_in);
    }

    if (f_out) {
        fclose(f_out);
    }
}

TEST(cut_matrix_creation, failure) {
    FILE* f_in = fopen("../tests/test_data/cut_odd_elements/testdata_cut_in2.dat", "r");

    Matrix* matrix = create_matrix_from_input(f_in);

    Matrix* cut_matrix_1 = cut_odd_elements(matrix);

    FILE* f_out = fopen("../tests/test_data/cut_odd_elements/testdata_cut_out2.dat", "r");

    Matrix* cut_matrix_2 = cut_matrix_creation(f_out);

    EXPECT_FALSE(cut_matrix_assertion(cut_matrix_1, cut_matrix_2));

    free_matrix(&cut_matrix_1);
    free_matrix(&cut_matrix_2);
    free_matrix(&matrix);

    if (f_in) {
        fclose(f_in);
    }

    if (f_out) {
        fclose(f_out);
    }
}

TEST(cut_matrix_creation, empty_cut_matrix_1) {
    FILE* f_in = fopen("../tests/test_data/cut_odd_elements/testdata_cut_empty_in1.dat", "r");

    Matrix* matrix = create_matrix_from_input(f_in);

    Matrix* cut_matrix_1 = cut_odd_elements(matrix);

    FILE* f_out = fopen("../tests/test_data/cut_odd_elements/testdata_cut_empty_out1.dat", "r");

    Matrix* cut_matrix_2 = cut_matrix_creation(f_out);

    EXPECT_TRUE(cut_matrix_assertion(cut_matrix_1, cut_matrix_2));

    free_matrix(&cut_matrix_1);
    free_matrix(&cut_matrix_2);
    free_matrix(&matrix);

    if (f_in) {
        fclose(f_in);
    }

    if (f_out) {
        fclose(f_out);
    }
}

TEST(cut_matrix_creation, empty_cut_matrix_2) {
    FILE* f_in = fopen("../tests/test_data/cut_odd_elements/testdata_cut_empty_in2.dat", "r");

    Matrix* matrix = create_matrix_from_input(f_in);

    Matrix* cut_matrix_1 = cut_odd_elements(matrix);

    FILE* f_out = fopen("../tests/test_data/cut_odd_elements/testdata_cut_empty_out2.dat", "r");

    Matrix* cut_matrix_2 = cut_matrix_creation(f_out);

    EXPECT_TRUE(cut_matrix_assertion(cut_matrix_1, cut_matrix_2));

    free_matrix(&cut_matrix_1);
    free_matrix(&cut_matrix_2);
    free_matrix(&matrix);

    if (f_in) {
        fclose(f_in);
    }

    if (f_out) {
        fclose(f_out);
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
